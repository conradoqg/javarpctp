package com.googlecode.javarpctp.test.shared;

import com.googlecode.javarpctp.remote.RemoteService;

public interface FooService extends RemoteService {

	public String bar( Integer i );
	public String thrown() throws Exception;
}
