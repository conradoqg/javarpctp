package com.googlecode.javarpctp.test;

public class ConfigurationService {
    private static ConfigurationService defaultInstance;
    private int port;
        
    private ConfigurationService(int port) {
        this.port = port;
    }
    
    public static ConfigurationService getDefaultConfiguration() {
        if (defaultInstance == null) {
            defaultInstance = new ConfigurationService(5000);         
        }
        return defaultInstance;
    }

    public int getPort() {
        return port;
    }
}
