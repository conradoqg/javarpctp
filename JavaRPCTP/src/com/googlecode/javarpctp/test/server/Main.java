package com.googlecode.javarpctp.test.server;

import com.googlecode.javarpctp.test.common.LoggedEvent;
import com.googlecode.javarpctp.test.common.ThreadCreatedEvent;
import com.googlecode.javarpctp.test.common.ThreadStateChangedEvent;
import com.googlecode.javarpctp.test.shared.FooService;
import com.googlecode.javarpctp.server.Server;
import com.googlecode.javarpctp.server.ServerState;
import com.googlecode.javarpctp.server.ServerStateChangedListener;
import java.io.IOException;
import java.util.logging.Level;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.table.DefaultTableModel;
import com.googlecode.javarpctp.test.common.LoggedListener;
import com.googlecode.javarpctp.test.common.Logger;
import com.googlecode.javarpctp.test.common.ThreadListener;
import com.googlecode.javarpctp.test.common.ThreadMonitor;
import com.googlecode.javarpctp.test.common.ThreadMonitorListener;
import com.googlecode.javarpctp.communication.ConnectionHandlerMessageTransportEvent;
import com.googlecode.javarpctp.communication.ConnectionHandlerMessageTransportListener;
import com.googlecode.javarpctp.communication.ConnectionThread;
import com.googlecode.javarpctp.communication.StopCause;
import com.googlecode.javarpctp.server.ServerStateChangedEvent;
import com.googlecode.javarpctp.test.ConfigurationService;
import java.util.ArrayList;

public class Main extends javax.swing.JFrame {

    private Server server;
    private Thread serverThread;
    private ThreadMonitor threadMonitor = new ThreadMonitor();

    /** Creates new form Main */
    public Main() {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (UnsupportedLookAndFeelException e) {
            // handle exception
        } catch (ClassNotFoundException e) {
            // handle exception
        } catch (InstantiationException e) {
            // handle exception
        } catch (IllegalAccessException e) {
            // handle exception
        }

        initComponents();

        this.server = new Server(ConfigurationService.getDefaultConfiguration().getPort());

        this.setLocationRelativeTo(null);

        this.server.addServerStateChangedListener(new ServerStateChangedListener() {

            @Override
            public void stateChanged(ServerStateChangedEvent evt) {
                updateButtonByServerState(evt.getState());
                Server source = (Server) evt.getSource();
                if (source.getStopCause() == StopCause.EXCEPTION) {
                    JOptionPane.showMessageDialog(
                            Main.this,
                            "Server has been disconnected by exception " + source.getStopException().getMessage(),
                            "Attention",
                            JOptionPane.ERROR_MESSAGE);

                }
            }
        });
        this.server.addThreadListener(new ThreadListener() {

            @Override
            public void threadCreated(ThreadCreatedEvent evt) {
                threadMonitor.addThreadToMonitor(evt.getThread());
            }
        });
        this.server.addConnectionHandlerMessageTransportListener(new ConnectionHandlerMessageTransportListener() {

            @Override
            public void messageTransported(final ConnectionHandlerMessageTransportEvent evt) {
                java.awt.EventQueue.invokeLater(new Runnable() {

                    @Override
                    public void run() {
                        addMessageToHistory(evt);
                    }
                });
            }
        });

        this.threadMonitor.addThreadMonitorListener(new ThreadMonitorListener() {

            @Override
            public void threadStateChanged(final ThreadStateChangedEvent evt) {
                java.awt.EventQueue.invokeLater(new Runnable() {

                    @Override
                    public void run() {
                        updateOrAddThread(evt.getThread());
                    }
                });
            }
        });
        this.threadMonitor.start();

        Logger.getInstance().addLoggedListener(new LoggedListener() {

            @Override
            public void logged(LoggedEvent evt) {
                final String finalMessage = evt.getMessage();
                final Level finalLevel = evt.getLevel();
                final Throwable finalThrown = evt.getThrown();

                java.awt.EventQueue.invokeLater(new Runnable() {

                    @Override
                    public void run() {
                        if (finalThrown != null) {
                            jConsole.append(finalThrown.getMessage() + '\n');
                        } else {
                            jConsole.append(finalMessage + "\n");
                        }
                        java.util.logging.Logger.getLogger(Main.class.getName()).log(finalLevel, finalMessage, finalThrown);
                    }
                });
            }
        });

        this.server.registerImplementation(FooService.class, new FooServiceImpl());
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel1 = new javax.swing.JPanel();
        jStartButton = new javax.swing.JButton();
        jStopButton = new javax.swing.JButton();
        jSplitPane1 = new javax.swing.JSplitPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        jConsole = new javax.swing.JTextArea();
        jSplitPane2 = new javax.swing.JSplitPane();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jThreads = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jMessages = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Server");
        getContentPane().setLayout(new java.awt.GridBagLayout());

        jPanel1.setAlignmentY(0.0F);
        jPanel1.setPreferredSize(new java.awt.Dimension(100, 0));
        jPanel1.setLayout(new javax.swing.BoxLayout(jPanel1, javax.swing.BoxLayout.Y_AXIS));

        jStartButton.setText("Start server");
        jStartButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jStartButtonActionPerformed(evt);
            }
        });
        jPanel1.add(jStartButton);

        jStopButton.setText("Stop server");
        jStopButton.setEnabled(false);
        jStopButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jStopButtonActionPerformed(evt);
            }
        });
        jPanel1.add(jStopButton);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        getContentPane().add(jPanel1, gridBagConstraints);

        jSplitPane1.setDividerLocation(150);
        jSplitPane1.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        jSplitPane1.setPreferredSize(new java.awt.Dimension(600, 600));

        jScrollPane1.setViewportBorder(javax.swing.BorderFactory.createTitledBorder("Console"));

        jConsole.setBackground(new java.awt.Color(0, 0, 0));
        jConsole.setColumns(20);
        jConsole.setEditable(false);
        jConsole.setForeground(new java.awt.Color(255, 255, 255));
        jConsole.setRows(5);
        jScrollPane1.setViewportView(jConsole);

        jSplitPane1.setLeftComponent(jScrollPane1);

        jSplitPane2.setDividerLocation(190);
        jSplitPane2.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Threads"));
        jPanel2.setLayout(new javax.swing.BoxLayout(jPanel2, javax.swing.BoxLayout.LINE_AXIS));

        jThreads.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Name", "State"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Long.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(jThreads);
        jThreads.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);

        jPanel2.add(jScrollPane2);

        jSplitPane2.setLeftComponent(jPanel2);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Messages transported"));
        jPanel3.setLayout(new javax.swing.BoxLayout(jPanel3, javax.swing.BoxLayout.LINE_AXIS));

        jMessages.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Client", "Class type", "Direction", "Data"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane3.setViewportView(jMessages);
        jMessages.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);

        jPanel3.add(jScrollPane3);

        jSplitPane2.setRightComponent(jPanel3);

        jSplitPane1.setBottomComponent(jSplitPane2);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        getContentPane().add(jSplitPane1, gridBagConstraints);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jStartButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jStartButtonActionPerformed
        if (this.serverThread == null || !this.serverThread.isAlive()) {
            this.serverThread = new Thread(this.server, "Connection listener");
            this.threadMonitor.addThreadToMonitor(serverThread);
            this.serverThread.start();
        } else {
            JOptionPane.showMessageDialog(this, "Servidor em execução", "Atenção!", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_jStartButtonActionPerformed

    private void jStopButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jStopButtonActionPerformed
        if (this.serverThread != null) {
            try {
                this.server.stop();
            } catch (IOException ex) {
                java.util.logging.Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
            this.serverThread.interrupt();
        } else {
            JOptionPane.showMessageDialog(this, "Servidor já está parado", "Atenção!", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_jStopButtonActionPerformed

    private void updateButtonByServerState(ServerState serverState) {
        switch (serverState) {
            case RUNNING:
                this.jStartButton.setEnabled(false);
                this.jStopButton.setEnabled(true);
                break;
            case STOPED:
                this.jStartButton.setEnabled(true);
                this.jStopButton.setEnabled(false);
                break;
        }
    }

    private void addMessageToHistory(ConnectionHandlerMessageTransportEvent evt) {
        ((DefaultTableModel) this.jMessages.getModel()).addRow(new String[]{((ConnectionThread) evt.getSource()).getName(), evt.getMessage().getClass().getSimpleName(), evt.getMessageDirection().name(), evt.getMessage().toString()});
        this.jMessages.repaint();
    }

    private void updateOrAddThread(Thread thread) {
        int rowCount = ((DefaultTableModel) this.jThreads.getModel()).getRowCount();
        ArrayList<Integer> rowsToRemove = new ArrayList<Integer>();
        boolean threadFound = false;
        for (int i = 0; i < rowCount; i++) {
            Long threadID = (Long) ((DefaultTableModel) this.jThreads.getModel()).getValueAt(i, 0);
            if (threadID.equals(thread.getId())) {
                threadFound = true;
                if (thread.getState() == Thread.State.TERMINATED) {
                    rowsToRemove.add(i);
                } else {
                    ((DefaultTableModel) this.jThreads.getModel()).setValueAt(thread.getState(), i, 2);
                }
            }
        }
        if (!threadFound) {
            ((DefaultTableModel) this.jThreads.getModel()).addRow(new Object[]{thread.getId(), thread.getName(), thread.getState().toString()});
        } else {
            for (Integer row : rowsToRemove) {
                ((DefaultTableModel) this.jThreads.getModel()).removeRow(row);
            }
        }
        this.jThreads.repaint();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                new Main().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea jConsole;
    private javax.swing.JTable jMessages;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JSplitPane jSplitPane2;
    private javax.swing.JButton jStartButton;
    private javax.swing.JButton jStopButton;
    private javax.swing.JTable jThreads;
    // End of variables declaration//GEN-END:variables
}
