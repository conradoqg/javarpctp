package com.googlecode.javarpctp.test.server;

import com.googlecode.javarpctp.test.shared.FooService;

public class FooServiceImpl implements FooService {

    @Override
    public String bar(Integer i) {
        System.out.println(i * 2);
        return String.valueOf(i * 2);
    }

    @Override
    public String thrown() throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
