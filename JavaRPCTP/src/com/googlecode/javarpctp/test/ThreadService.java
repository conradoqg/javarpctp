package com.googlecode.javarpctp.test;

import java.util.ArrayList;

public class ThreadService {
    private ArrayList<Thread> threads = new ArrayList<Thread>();    
    
    private ThreadService() {
    }
    
    public static ThreadService getInstance() {
        return ThreadServiceHolder.INSTANCE;
    }
    
    private static class ThreadServiceHolder {

        private static final ThreadService INSTANCE = new ThreadService();
    }
    
    public void add(Thread thread) {
        synchronized (this) {
            threads.add(thread); 
        }                
    }
    
    public void remove(Thread thread) {
        synchronized (this) {
            threads.add(thread);
        }
    }
    
    public ArrayList<Thread> getThreads() {
        return threads;
    }
}
