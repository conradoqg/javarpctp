package com.googlecode.javarpctp.test.common;

import java.util.EventListener;

public interface LoggedListener extends EventListener {
    public void logged(LoggedEvent evt);
}
