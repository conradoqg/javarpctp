package com.googlecode.javarpctp.test.common;

import java.util.EventObject;

public class ThreadCreatedEvent extends EventObject {
    private Thread thread;

    public ThreadCreatedEvent(Object source) {
        super(source);
    }

    public ThreadCreatedEvent(Object source, Thread thread) {
        super(source);
        this.thread = thread;
    }

    public Thread getThread() {
        return thread;
    }
}
