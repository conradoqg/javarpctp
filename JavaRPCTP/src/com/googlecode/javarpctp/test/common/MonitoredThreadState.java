package com.googlecode.javarpctp.test.common;

public class MonitoredThreadState {

    private Thread thread;
    private Thread.State lastState;

    public MonitoredThreadState(Thread thread, Thread.State lastState) {
        this.thread = thread;
        this.lastState = lastState;
    }

    public Thread getThread() {
        return thread;
    }

    public Thread.State getLastState() {
        return lastState;
    }

    public void setLastState(Thread.State lastState) {
        this.lastState = lastState;
    }
}
