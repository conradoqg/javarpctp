package com.googlecode.javarpctp.test.common;

import java.util.EventObject;
import java.util.logging.Level;

public class LoggedEvent extends EventObject {

    private Level level;
    private String message;
    private Throwable thrown;

    public LoggedEvent(Object source) {
        super(source);
    }

    public LoggedEvent(Object source, Level level, String message, Throwable thrown) {
        this(source);
        this.level = level;
        this.message = message;
        this.thrown = thrown;
    }

    public Level getLevel() {
        return level;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getThrown() {
        return thrown;
    }

}
