package com.googlecode.javarpctp.test.common;

import java.util.logging.Level;
import javax.swing.event.EventListenerList;

public class Logger {
    protected EventListenerList listenerList = new EventListenerList();
    
    private Logger() {
    }
    
    public static Logger getInstance() {
        return LoggerHolder.INSTANCE;
    }
    
    private static class LoggerHolder {

        private static final Logger INSTANCE = new Logger();
    }
    
    public void log(Level level, String message, Throwable thrown) {
        this.fireLogged(new LoggedEvent(this, level, message, thrown));
    }
    
    //<editor-fold defaultstate="collapsed" desc="Events">
    public void addLoggedListener(LoggedListener listener ) {
        this.listenerList.add(LoggedListener.class, listener);
    }
    
    public void removeLoggedListener(LoggedListener listener) {
        this.listenerList.remove(LoggedListener.class, listener);
    }
    
    void fireLogged(LoggedEvent evt) {
        Object[] listeners = this.listenerList.getListenerList();
        // Each listener occupies two elements - the first is the listener class
        // and the second is the listener instance
        for (int i=0; i<listeners.length; i+=2) {
            if (listeners[i]==LoggedListener.class) {
                ((LoggedListener)listeners[i+1]).logged(evt);
            }
        }
    }
    //</editor-fold>
}
