package com.googlecode.javarpctp.test.common;

import java.util.EventListener;

public interface ThreadListener extends EventListener {
    public void threadCreated(ThreadCreatedEvent evt);
}
