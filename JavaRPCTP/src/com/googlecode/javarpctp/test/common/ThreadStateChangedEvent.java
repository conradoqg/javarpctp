package com.googlecode.javarpctp.test.common;

import java.util.EventObject;

public class ThreadStateChangedEvent extends EventObject {
    private Thread thread;
    private Thread.State fromState;
    private Thread.State toState;

    public ThreadStateChangedEvent(Object source) {
        super(source);
    }

    public ThreadStateChangedEvent(Object source, Thread thread, Thread.State fromState, Thread.State toState) {
        this(source);
        this.thread = thread;
        this.fromState = fromState;
        this.toState = toState;
    }

    public Thread getThread() {
        return thread;
    }

    public Thread.State getFromState() {
        return fromState;
    }

    public Thread.State getToState() {
        return toState;
    }  
}
