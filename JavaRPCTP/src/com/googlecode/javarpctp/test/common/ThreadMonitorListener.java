package com.googlecode.javarpctp.test.common;

import java.util.EventListener;

public interface ThreadMonitorListener extends EventListener {
    public void threadStateChanged(ThreadStateChangedEvent evt);    
}
