package com.googlecode.javarpctp.remote;

import com.googlecode.javarpctp.communication.ResponseMessage;
import com.googlecode.javarpctp.communication.ResponsiveMessage;

public class InvocationResponsiveMessage extends ResponsiveMessage {
    private InvocationData invocationData;
    
    public InvocationResponsiveMessage(InvocationData invocationData) {
        super();
        this.invocationData = invocationData;
    }
    
    public InvocationResponsiveMessage(InvocationData invocationData, ResponseMessage response) {
        super(response);
        this.invocationData = invocationData;
    }

    public InvocationData getInvocationData() {
        return invocationData;
    }

    @Override
    public String toString() {
        return getMessageID() + ": " + invocationData.toString();
    }  
}
