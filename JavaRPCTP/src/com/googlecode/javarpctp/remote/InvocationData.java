package com.googlecode.javarpctp.remote;

import java.io.Serializable;

public class InvocationData implements Serializable {

    private MethodSignature methodSignature;
    private Object[] args;

    public InvocationData(MethodSignature methodSignature, Object[] args) {
        this.methodSignature = methodSignature;
        this.args = args;
    }

    public Object[] getArgs() {
        return args;
    }

    public void setArgs(Object[] args) {
        this.args = args;
    }

    public MethodSignature getMethodSignature() {
        return methodSignature;
    }

    public void setMethodSignature(MethodSignature methodSignature) {
        this.methodSignature = methodSignature;
    }

    @Override
    public String toString() {
        return this.methodSignature.getInterfaceClassSimpleName() + "." + this.methodSignature.getMethodName() + "(" + argsToString(args) + ")";
    }

    private static String argsToString(Object[] args) {
        StringBuilder result = new StringBuilder();
        if (args != null && args.length > 0) {
            result.append(args[0]);
            for (int i = 1; i < args.length; i++) {
                result.append(",");
                result.append(args[i]);
            }
        }
        return result.toString();
    }
}
