package com.googlecode.javarpctp.remote;

import com.googlecode.javarpctp.communication.ResponseMessage;

public class CallMessage extends InvocationResponsiveMessage {    
    
    public CallMessage(InvocationData invocationData) {
        super(invocationData);
    }
    
    public CallMessage(InvocationData invocationData, ResponseMessage response) {
        super(invocationData,response);        
    }    
}
