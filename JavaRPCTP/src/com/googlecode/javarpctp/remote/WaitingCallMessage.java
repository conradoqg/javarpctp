package com.googlecode.javarpctp.remote;

import com.googlecode.javarpctp.communication.ResponsiveMessage;

public class WaitingCallMessage {
    private ResponsiveMessage responsiveMessage;
    private Thread waitingThread;    
    
    public WaitingCallMessage(ResponsiveMessage responsiveMessage, Thread waitingThread) {
        this.responsiveMessage = responsiveMessage;
        this.waitingThread = waitingThread;
    }

    public ResponsiveMessage getResponsiveMessage() {
        return responsiveMessage;
    }

    public Thread getWaitingThread() {
        return waitingThread;
    }
}
