package com.googlecode.javarpctp.remote;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class MethodSignature implements Serializable {
	private String interfaceClass;
        private String interfaceClassSimpleName;
	private String methodName;
	private List<String> parameterClasses;
	
	public MethodSignature( Method method ) {
		this.interfaceClass = method.getDeclaringClass().getName();
                this.interfaceClassSimpleName = method.getDeclaringClass().getSimpleName();
		this.methodName = method.getName();
		this.parameterClasses = new ArrayList<String>();
		for( Class<?> paramClass : method.getParameterTypes() ) {
			this.parameterClasses.add( paramClass.getName() );
		}
	}
	
	public Method resolveMethod() throws ClassNotFoundException, SecurityException, NoSuchMethodException {
		Class<?> ifClass = Class.forName( getInterfaceClass());
		Class<?> [] paramClasses = new Class<?>[this.parameterClasses.size() ];
		for( int i = 0; i < this.parameterClasses.size(); i++ ) {
			paramClasses[ i ] = Class.forName( parameterClasses.get( i ) ); 
		}
		Method method = ifClass.getDeclaredMethod(getMethodName(), paramClasses);
		return method;
		
	}

    public String getMethodName() {
        return methodName;
    }

    public String getInterfaceClass() {
        return interfaceClass;
    }

    public String getInterfaceClassSimpleName() {
        return interfaceClassSimpleName;
    }
}