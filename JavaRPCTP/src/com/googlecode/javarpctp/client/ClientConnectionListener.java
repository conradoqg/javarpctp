package com.googlecode.javarpctp.client;

import java.util.EventListener;
import java.util.EventObject;

public interface ClientConnectionListener extends EventListener {
    public void connected(EventObject evt);
    public void disconnected(EventObject evt);
}
