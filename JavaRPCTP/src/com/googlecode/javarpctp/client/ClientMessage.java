package com.googlecode.javarpctp.client;

import com.googlecode.javarpctp.communication.Message;

public class ClientMessage extends Message {

    private ClientCode clientCode;

    public ClientMessage(ClientCode clientCode) {
        this.clientCode = clientCode;
    }

    public ClientCode getClientCode() {
        return clientCode;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + ": " + this.clientCode.name();
    }
}
