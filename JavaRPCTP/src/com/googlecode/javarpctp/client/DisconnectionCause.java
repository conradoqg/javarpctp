package com.googlecode.javarpctp.client;

public enum DisconnectionCause {
    REQUESTED,
    EXCEPTION,
    UNKNOWN
}
