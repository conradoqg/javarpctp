package com.googlecode.javarpctp.client;

import java.util.EventObject;

public class ClientDisconnectedEvent extends EventObject {
    private DisconnectionCause disconnectionCause;
    private Exception disconnectionException;

    public ClientDisconnectedEvent(Object source, DisconnectionCause disconnectionCause) {
        super(source);
        this.disconnectionCause = disconnectionCause;
    }
    
    public ClientDisconnectedEvent(Object source, DisconnectionCause disconnectionCause, Exception disconnectionException) {
        super(source);
        this.disconnectionCause = disconnectionCause;
        this.disconnectionException = disconnectionException;
    }

    public DisconnectionCause getDisconnectionCause() {
        return disconnectionCause;
    }

    public Exception getDisconnectionException() {
        return disconnectionException;
    }
}
