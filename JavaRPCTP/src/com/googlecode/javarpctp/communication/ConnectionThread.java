package com.googlecode.javarpctp.communication;

public class ConnectionThread extends Thread {
    private int connectionID;
    private ConnectionHandler connectionHandler; 
    
    public ConnectionThread(ConnectionHandler handler) {        
        this.connectionID = 0;        
        this.connectionHandler = handler;
    }
    
    public ConnectionThread(int connectionID, ConnectionHandler handler) {
        super(handler, "Connection handler " + connectionID);
        this.connectionID = connectionID;        
        this.connectionHandler = handler;
    }
    
    public ConnectionThread(ConnectionHandler handler, String name) {
        super(handler, name);
        this.connectionID = 0;        
        this.connectionHandler = handler;        
    }

    public ConnectionHandler getConnectionHandler() {
        return connectionHandler;
    }

    public int getConnectionID() {
        return connectionID;
    }
}
