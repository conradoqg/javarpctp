package com.googlecode.javarpctp.communication;

import java.util.EventObject;

public class ConnectionStateChangedEvent extends EventObject {
    
    private ConnectionState state;

    public ConnectionStateChangedEvent(Object source) {
        super(source);
    }

    public ConnectionStateChangedEvent(Object source, ConnectionState state) {
        this(source);
        this.state = state;
    }

    public ConnectionState getState() {
        return state;
    }
}
