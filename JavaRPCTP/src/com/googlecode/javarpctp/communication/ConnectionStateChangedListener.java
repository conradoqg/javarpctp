package com.googlecode.javarpctp.communication;

import java.util.EventListener;

public interface ConnectionStateChangedListener extends EventListener {
    public void stateChanged(ConnectionStateChangedEvent evt);
}
