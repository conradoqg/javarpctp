package com.googlecode.javarpctp.communication;

public enum ConnectionState {
    CREATED,    
    RUNNING,
    STOPPING,
    STOPED
}
