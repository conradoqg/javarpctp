package com.googlecode.javarpctp.communication;

import java.util.EventListener;

public interface CustomMessageRecievedListener extends EventListener {
    public void customMessageRecieved(CustomMessageRecievedEvent evt) throws Exception ;
}
