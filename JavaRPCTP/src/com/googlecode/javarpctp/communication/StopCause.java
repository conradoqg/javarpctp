package com.googlecode.javarpctp.communication;

public enum StopCause {
    REQUESTED,
    EXCEPTION,
    UNKOWN
}