package com.googlecode.javarpctp.communication;

public class ResponseMessage extends Message {
    private Throwable thrown;
    private Object data;
    private int messageID;
    
    public ResponseMessage(int messageID, Object data) {
        this.messageID = messageID;
        this.data = data;
    }
    
    public ResponseMessage(int messageID, Throwable thrown) {
        this.messageID = messageID;
        this.thrown = thrown;
    }

    public Throwable getThrown() {
        return thrown;
    }

    public Object getData() {
        return data;
    }

    public int getMessageID() {
        return messageID;
    }

    @Override
    public String toString() {
        return getMessageID() + ": " + (getThrown() == null ? data.getClass().getSimpleName() : "Exception: " + getThrown().getCause().getMessage());
    }   
}
