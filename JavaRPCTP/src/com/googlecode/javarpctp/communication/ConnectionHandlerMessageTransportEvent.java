package com.googlecode.javarpctp.communication;

import java.util.EventObject;

public class ConnectionHandlerMessageTransportEvent extends EventObject {
    private MessageDirection messageDirection;
    private Message message;
    
    public ConnectionHandlerMessageTransportEvent(Object source) {
        super(source);
    }

    public ConnectionHandlerMessageTransportEvent(Object source, MessageDirection messageDirection, Message message) {
        this(source);
        this.messageDirection = messageDirection;
        this.message = message;
    }

    public MessageDirection getMessageDirection() {
        return messageDirection;
    }

    public Message getMessage() {
        return message;
    }    
}
