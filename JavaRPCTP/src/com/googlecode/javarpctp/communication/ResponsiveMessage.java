package com.googlecode.javarpctp.communication;

public class ResponsiveMessage extends NumberedMessage {
    ResponseMessage response;
    
    public ResponsiveMessage() {
        super();     
    }

    public ResponsiveMessage(ResponseMessage response) {
        super();
        this.response = response;
    }

    public ResponseMessage getResponse() {
        return response;
    }

    public void setResponse(ResponseMessage response) {
        this.response = response;
    }        
}
