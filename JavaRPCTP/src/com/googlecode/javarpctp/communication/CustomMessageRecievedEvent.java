package com.googlecode.javarpctp.communication;

import java.util.EventObject;

public class CustomMessageRecievedEvent extends EventObject {

    private Message message;

    public CustomMessageRecievedEvent(Object source) {
        super(source);
    }

    public CustomMessageRecievedEvent(Object source, Message message) {
        this(source);
        this.message = message;
    }

    public Message getMessage() {
        return message;
    }
}
