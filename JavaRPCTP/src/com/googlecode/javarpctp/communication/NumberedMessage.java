package com.googlecode.javarpctp.communication;

import java.util.concurrent.atomic.AtomicInteger;

public class NumberedMessage extends Message {    
    private static AtomicInteger messageIDCounter = new AtomicInteger(0);
    private int messageID = 0;
            
    public NumberedMessage() {
        this.messageID = NumberedMessage.messageIDCounter.getAndIncrement();        
    }    

    public int getMessageID() {
        return messageID;
    }
}
