package com.googlecode.javarpctp.communication;

import java.util.EventListener;

public interface ConnectionHandlerMessageTransportListener extends EventListener {
    public void messageTransported(ConnectionHandlerMessageTransportEvent evt);
}
