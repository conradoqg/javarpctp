package com.googlecode.javarpctp.server;

import java.util.EventListener;

public interface ServerStateChangedListener extends EventListener {
    public void stateChanged(ServerStateChangedEvent evt);
}
