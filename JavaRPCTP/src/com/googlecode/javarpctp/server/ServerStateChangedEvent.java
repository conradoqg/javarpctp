package com.googlecode.javarpctp.server;

import java.util.EventObject;

public class ServerStateChangedEvent extends EventObject {

    private ServerState state;

    public ServerStateChangedEvent(Object source) {
        super(source);
    }

    public ServerStateChangedEvent(Object source, ServerState state) {
        this(source);
        this.state = state;
    }

    public ServerState getState() {
        return state;
    }
}
