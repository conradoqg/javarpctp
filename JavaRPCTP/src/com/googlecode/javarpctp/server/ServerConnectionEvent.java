package com.googlecode.javarpctp.server;

import com.googlecode.javarpctp.communication.ConnectionThread;
import java.util.EventObject;


public class ServerConnectionEvent extends EventObject {
    private ConnectionThread connectionThread;

    public ServerConnectionEvent(Object source, ConnectionThread connectionThread) {
        super(source);
        this.connectionThread = connectionThread;
    }

    public ConnectionThread getConnectionThread() {
        return connectionThread;
    }
    
}
