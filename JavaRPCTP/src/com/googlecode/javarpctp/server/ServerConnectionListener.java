package com.googlecode.javarpctp.server;

import java.util.EventListener;

public interface ServerConnectionListener extends EventListener {
    public void clientConnected(ServerConnectionEvent evt);
    public void clientDisconnected(ServerConnectionEvent evt);
}
