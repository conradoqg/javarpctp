package com.googlecode.javarpctp.server;

import com.googlecode.javarpctp.communication.Message;

public class ServerMessage extends Message {

    private ServerCode serverCode;

    public ServerMessage(ServerCode serverCode) {
        this.serverCode = serverCode;
    }

    public ServerCode getServerCode() {
        return serverCode;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + ": " + this.serverCode.name();
    }
}
