package com.googlecode.javarpctp.server;

public enum ServerState {
    CREATED,    
    RUNNING, 
    STOPPING,
    STOPED
}
